#JavaScript Course#

##Backend 18.07##

-	JSON understanding: Sa adaugati in fisierul MockedListData.json, fiecarui element al fiecarei liste o noua proprietate numita body ce va contine o scurta descriere a taskului din titlu (continutul conteaza mai putin).
-	Element retrieval: O noua metoda de tip GET ce ne va returna un element al unei liste cu urmatoarele elemente componente: title si body.
-	Adding elements to a list: In momentul de fata aplicatia isi incarca la rulare un set de date mocked (din fisierul MockedListData.json) cu care noi ne putem juca, ce se vor reseta la un restart al aplicatiei, acestea aflandu-se in memoria volatila. Variabila globala mockedData poate fi alterata de apelurile successive ale clientilor. Va trebuie sa creati o noua metoda de tip POST care sa adauge un nou listElement unei liste, avand ca date furnizate de client: listId, elementId, title si body (aceasta functionalitate poate fi testate cu ajutorul celorlalte Metode din API).
-	Remove elements from a list: O noua metoda de tip DELETE ce ar avea ca scop stergerea unui listElement pe baza id-ului listei si al elementului.


##Frontend 19.07##

- Angular counter

##Backend 23.07##

-	Model understanding: Sa adaugati la ListElement inca o proprietate care sa se numeasca “done”, de tip Boolean, si care sa reprezinte finalitatea task-ului. Daca task-ul e finalizat, o sa fie true, daca nu, o sa fie false.
-	Element retrieval: Sa adaptati metoda GET care returneaza un element al listei ca acum sa il returneze direct din baza de date.
-	Remove elements from list: Sa adaptati metoda DELETE ca sa stearga un element dintr-o lista din baza de date.
-	Remove list: Sa creati o noua metoda de tip DELETE care va avea ca scop stergerea unei intregi liste din baza de date.


##Frontend 26.07##

-   Sa va dati seama cum ati putea evidentia / highlightui lista selectata (clasa care modifica aspectul e “is-active”.
-   Sa creati un component care sa reprezinte lista selectata din meniu
-	Sa gasiti o solutie prin care sa sincronizati lista selectata din stanga cu componentul creat in dreapta

##Backend 2.08

-	Register new user: va trebui sa scrieti codul necesar inregistrarii unui nou user in baza de date
-	Encryption: in momentul de fata parolele sunt salvate plain text, ceea ce nu ofera Securitate. Va trebui sa hashuiti parola inainte sa o salvati in baza de date (la register) si de acum inainte va trebui sa comparati hash-urile la login. Pentru punctul asta v-am recomandat sa folositi bcrypt, care are deja aceste metode implmentate ca sa nu trebuiasca sa scrieti voi algoritmii.
-	Reset password: pe baza unor intrebari si raspunsuri setate la inregistrarea unui nou utilizator, va trebui sa suportati functionalitatea de resetare a parolei pe baza raspunsurilor corecte la intrebarile respective. Practic, daca un user vrea sa isi reseteze parola va trebui sa isi introduca email-ul si apoi sa raspunda correct le intrebari, dupa care va trebui sa isi introduca noua parola pe baza careia se va loga de acum incolo
-	Lists for specific user: colectiile de Users si ToDoLists trebuie sa fie legate in asa fel incat GET /lists sa returneze doar listele utilizatorului logat.

##Frontend 6.08
