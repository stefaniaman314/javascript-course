import express from "express";
import bodyParser from "body-parser";
import * as mockedData from "../resources/MockedListData.json";

const app = express();
const port = 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//========== GET METHODS ==========
app.get("/", (request, response) => {
  response.send("Hello, world");
});

app.get("/lists", (req, res) => {
  res.send(mockedData);
});

app.get("/list/:id", (req, res) => {
  let idFromRequest = req.params.id;
  let requestList = mockedData.lists.filter(el => el.id === idFromRequest);
  
  if(requestList) {
    res.send(requestList);
  } else {
    res.send("Oopsie! No list found.");
  }
  
});

//returns a to do by id
app.get("/toDo/:idList/:idToDo", (req, res) => {
  // let idListReq = req.params.idList;
  // let idElementReq = req.params.idElement;
  // let element = mockedData.lists[idListReq].listElements[idElementReq];
  // let reqElement = { title: element.title, body: element.body };
  // res.send(reqElement);
  
  let list = mockedData.lists.find(elem => elem.id = req.params.idList);

  if(list) {
    let element = list.listElements.find(elem => elem.id = req.params.idToDo);

    if(element) {
        res.send({ title: element.title, body: element.body });
    } else {
      res.send("No element found.");
    } 
  } else {
    res.send("")
  }
});

//returns all to dos
app.get("/toDos", (request, response) => {
  let lists = mockedData.lists;
  let toDos: { "title:": string; "body:": string }[] = [];

  lists.forEach(list => {
    list.listElements.forEach(element => {
      toDos.push({
        "title:": element.title,
        "body:": element.body
      });
    });
  });

  response.send(toDos);
});

//========== POST METHODS ==========
app.post("/toConsole", (request, response) => {
  console.log(request.body.myMessage);
});

app.post("/new/:listId/:elementId/:title/:body", (request, response) => {
  let lists = mockedData.lists;
  let listId = request.params.listId;

  lists[listId].listElements.push({
    id: request.params.elementId,
    title: request.params.title,
    body: request.params.body
  });

  response.send(lists);
});

app.post("/newElement", (req, res) => {
	let list = mockedData.lists.find(el => el.id === req.body.listId);
	if (list) {
		list.listElements.push({
			id: req.body.elementId,
			title: req.body.title,
			body: req.body.body
		});
		res.send("Element inserted!");
	} else {
		res.send("No list found.");
	}
});

//========== DELETE METHODS ==========
app.delete("/deleteElement/:listId/:listElementId", (req, res) => {
	let list = mockedData.lists.find(el => el.id === req.params.listId);
  
  if (list) {
    let element = list.listElements.find(el => el.id = req.params.listElementId);
    
		if(element) {
      list.listElements = list.listElements.filter(elem => { elem !== element});
      res.send(mockedData.lists);
    } else {
      res.send("Element not found!");
    }		
	} else {
		res.send("No list found");
	}
});

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}.`);
});
