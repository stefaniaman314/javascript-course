export default class ListElement {
    title: string;
    body: string;
}