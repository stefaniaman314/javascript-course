import { Component, OnInit } from '@angular/core';
import { TodoService } from './services/todo/todo.service';
import List from './models/List';
import ListElement from './models/ListElement';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {  

  // constructor (private todoService: TodoService) { }

  lists: List[];

  private todoService: TodoService;
  constructor(todoService: TodoService) {
    this.todoService = todoService;
  }

  async ngOnInit() {
    this.lists = await this.todoService.getLists();
  }
}
